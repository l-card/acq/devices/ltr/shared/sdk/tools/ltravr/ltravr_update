#include "ltr/include/ltravrapi.h"
#include "ltr/include/ltrapi.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "getopt.h"
#include "crc/crc.h"


#define OPT_HELP       'h'
#define OPT_VERBOSE    'v'
#define OPT_CRATE_SN   'c'
#define OPT_SLOT       's'
#define OPT_ALL        0x100
#define OPT_SET_SERIAL 0x101



#define ERR_INVALID_USAGE           -1
#define ERR_OPEN_FILE               -2
#define ERR_FILE_READ               -3
#define ERR_UNSUP_FIRM_FILE         -4
#define ERR_MODULE_INFO             -5
#define ERR_FIRM_VERIFY             -6
#define ERR_EMPTY_SLOT              -7
#define ERR_IDENTIFYING_SLOT        -8
#define ERR_INVALID_MID_IN_SLOT     -9
#define ERR_INVALID_FILE_SIZE       -10


#define MAX_INFO_SIZE   (4800 + 128)
#define MAX_USERDATA_SIZE  0x800

typedef enum {
    MODULE_FLAGS_CRC_BE    = 0x1, /* признак, что CRC хранится в big-endian формате */
    MODULE_FLAGS_VER_BE    = 0x2, /* признак, что версия хранится в big-endian формате */
    MODULE_FLAGS_CRC_FFFF  = 0x4, /* признак, что вместо crc хранятся все 1 */
} t_module_flags;



static const struct option f_long_opt[] = {
    {"help",         no_argument,       0, OPT_HELP},
    /* вывод сообщений о процессе загрузки */
    {"verbose",      no_argument,       0, OPT_VERBOSE},
    {"csn",          required_argument, 0, OPT_CRATE_SN},
    {"slot",         required_argument, 0, OPT_SLOT},
    {"all",          no_argument,       0, OPT_ALL},
#ifdef ENABLE_SET_SERIAL
    {"set-serial",   required_argument, 0, OPT_SET_SERIAL},
#endif
    {0,0,0,0}
};

static const char* f_opt_str = "hvc:s:";

typedef struct {
    int verbose;
    int all;
    int slot;
    const char *csn;
    const char *filename;
#ifdef ENABLE_SET_SERIAL
    const char *set_serial;
#endif
} t_ltrcfg_state;




typedef struct {
    WORD mid;
    const char* devname;
    DWORD info_offs;
    DWORD info_size;
    DWORD devname_offset;
    DWORD serial_offset;
    DWORD serial_size;
    DWORD cbr_offset;
    DWORD cbr_size;
    DWORD firm_ver_offset;
    DWORD firm_ver_size;
    DWORD firm_date_offset;
    DWORD firm_date_size;
    DWORD userdata_offset;
    DWORD userdata_size;
    DWORD flash_words;
    DWORD flags;
} t_module_param;


static t_module_param f_param_tbl[] = {
    {LTR_MID_LTR11,  "LTR11",   0x80,   75,   17, 25, 16, 41, 32,  1, 2, 3, 14, 0, 0, ATMEGA8515_FLASH_SIZE, MODULE_FLAGS_CRC_BE | MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR12,  "LTR12",   0x80,  320,   20, 28, 16, 52, 320 - 52 - 2,  4, 2, 6, 14, 0x1000, 0x800, ATMEGA8515_FLASH_SIZE, 0},
    {LTR_MID_LTR114, "LTR114",  0x80,   79,   17, 25, 16, 41, 36,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_CRC_BE | MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR27,  "LTR27", 0x1F80, 0x80,   16, 32, 16,  0,  0, 68, 4, 0, 0,  0, 0, ATMEGA128_FLASH_SIZE, 0},
    {LTR_MID_LTR41,  "LTR41",   0x80,   44,   17, 25, 17,  0,  0,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR42,  "LTR42",   0x80,   44,   17, 25, 17,  0,  0,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR43,  "LTR43",   0x80,   44,   17, 25, 17,  0,  0,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR22,  "LTR22", 0x1F80 - 4800, 4800+0x80, 4816, 4832, 16,  0,  4800, 4868, 4, 0, 0,  0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_CRC_FFFF},
};


static const char* f_usage_descr = \
"\nUsage: ltravrupdate [OPTIONS] firmware-file\n\n" \
" ltravrupdate is command line utility for update AVR firmware in LTR-modules.\n"
" This utility determinates by firmware-file for which module this firmware\n"
"   intended.\n"
" You can update firmware for all LTR-modulus for which it fit:\n\n"
"   ltravrupdate -v --all firmware-file\n\n"
" Or you can update firmware only for specified module:\n\n"
"   ltravrupdate -v --csn=crate-serial --slot=slot-in-crate fimware-file\n";

static const char* f_options_descr =
"Options:\n" \
"    --all               - Batch update firmware for all modules for wich\n"
"                          firmware fits\n"
"-c, --csn=crate-serial  - Crate serial which contains module, which needs to be\n"
"                          upgraded\n"
"-h, --help              - Print this help and exit\n"
#ifdef ENABLE_SET_SERIAL
"    --set-serial=serial - Set specified device serial number\n"
#endif
"-s, --slot=module-slot  - Slot number (1-16) of module, which needs to be\n"
"                          upgraded\n"
"-v, --verbose           - Print verbose information about process of upgrading.\n"
"                          Without this option utility prints only error\n"
"                          messages\n";


static void  f_print_usage(void) {
    unsigned int i;
    fprintf(stdout, "%s", f_usage_descr);
    fprintf(stdout, "\nSupported modules:\n");
    for (i=0; i < sizeof(f_param_tbl)/sizeof(f_param_tbl[0]); i++) {
        fprintf(stdout, "    %s\n", f_param_tbl[i].devname);
    }
    fprintf(stdout,"\n");
    fprintf(stdout, "%s", f_options_descr);
}

static int f_parse_options(t_ltrcfg_state* st, int argc, char **argv, int* out) {
    int err = 0;
    int opt = 0;

    *out = 0;
    st->csn = "";
#ifdef ENABLE_SET_SERIAL
    st->set_serial = NULL;
#endif

    /************************** разбор опций *****************************/
    while ((opt!=-1) && !err && !*out) {
        opt = getopt_long(argc, argv, f_opt_str,
                          f_long_opt, 0);
        switch (opt) {
            case -1:
                break;
            case OPT_HELP:
                f_print_usage();
                *out = 1;
                break;
            case OPT_VERBOSE:
                st->verbose = 1;
                break;
            case OPT_CRATE_SN:
                st->csn = optarg;
                break;
            case OPT_ALL:
                st->all = 1;
                break;
            case OPT_SLOT: {
                    int slot = atoi(optarg);
                    if ((slot < LTR_CC_CHNUM_MODULE1) || (slot > LTR_CC_CHNUM_MODULE16)) {
                        fprintf(stderr, "Error! Invalid slot number\n");
                        err = ERR_INVALID_USAGE;
                    } else {
                        st->slot = slot;
                    }
                }
                break;
#ifdef ENABLE_SET_SERIAL
            case OPT_SET_SERIAL:
                st->set_serial = optarg;
                break;
#endif
            default:
                break;
        }
    }

    if (!err && !*out) {
        if (optind >= argc) {
            fprintf(stderr, "Error! firmware file is not specified\n");
            err = ERR_INVALID_USAGE;
        } else {
            st->filename = argv[optind];
        }

#ifdef ENABLE_SET_SERIAL
        if (!err && st->all && (st->set_serial!=NULL)) {
            fprintf(stderr, "Cannot set same serial for all modules!\n");
            err = ERR_INVALID_USAGE;
        }
#endif
    }
    return err;
}


static BYTE f_info[MAX_INFO_SIZE];
static BYTE f_userdata[MAX_USERDATA_SIZE];


/* вывод версии прошивки из информации о модуле (используется смещения в параметрах).
 * сперва выводится версия (байты подряд через запятую), затем дата (если есть) */
static void f_print_firm_ver(BYTE* info, t_module_param* mpar) {
    if (mpar->firm_ver_size) {
        DWORD i;
        printf("ver = ");
        if (mpar->flags & MODULE_FLAGS_VER_BE) {
            for (i=0; i  < mpar->firm_ver_size; i++) {
                printf("%d", info[mpar->firm_ver_offset+i]);
                if (i != (mpar->firm_ver_size-1))
                    printf(".");
            }
        } else {
            for (i = mpar->firm_ver_size; i > 0; i--) {
                printf("%d", info[mpar->firm_ver_offset + i - 1]);
                if (i > 1)
                    printf(".");
            }
        }

        if (mpar->firm_date_size)
            printf(", ");
    }

    if (mpar->firm_date_size) {
        printf("date = %s", &info[mpar->firm_date_offset]);
    }
}

/* попытка определить, для какого модуля предназначена прошивка. в файле прошивки
 * идет попытка считать имя модуля и сравнить с требуемым */
static int f_get_info_from_firm(FILE* f, t_module_param** mpar, int verbose) {
    int err = 0, fnd = 0;
    unsigned i;
    size_t read_size;

    for (i=0; !fnd && (i < sizeof(f_param_tbl)/sizeof(f_param_tbl[0])); i++) {
        t_module_param* par = &f_param_tbl[i];
        fseek(f, par->info_offs, SEEK_SET);
        read_size = fread(f_info, 1, par->info_size, f);

        if ((WORD)read_size == par->info_size) {
            if (!strcmp(par->devname, (char*)&f_info[par->devname_offset])) {
                fnd = 1;
                *mpar = par;

                if (verbose) {
                    printf("Firmware for module %s. ", par->devname);
                    f_print_firm_ver(f_info, par);
                    printf("\n");
                    fflush(stdout);
                }
            }
        }
    }

    if (!fnd) {
        fprintf(stderr, "Error! Unsupported firmware file...\n");
        err = ERR_UNSUP_FIRM_FILE;
    }
    return err;
}

static void APIENTRY f_write_prog_vb(void* cb_data, TLTRAVR* hnd,
                                     DWORD done_size, DWORD full_size) {
    printf("."); fflush(stdout);
}

/* Запись прошивки в модуль. Функция записывает прошивку из файла, при этом сохраняя
 * серийный номер и калибровочные коэффициенты из модуля (предварительно их считав) и
 * включает в прошивку CRC для информации о модуле */
static int f_write_firm(const char* csn, int slot, FILE *f, t_module_param* mpar,
#ifdef ENABLE_SET_SERIAL
                        const char *set_serial,
#endif
                        int verbose) {
    int err = 0;
    int module_info_valid = 0;
    TLTRAVR hnd;
    LTRAVR_Init(&hnd);
    if (verbose) {
        printf("\nStart write firmware for module: crate = %s, slot = %d\n", csn, slot);
        printf("  Open module...        "); fflush(stdout);
    }

    err = LTRAVR_Open(&hnd, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, csn, slot);
    if (err == LTR_OK) {
        if (verbose) {
            printf("ok: crate = %s, slot = %d\n", hnd.ltr.csn, hnd.ltr.cc);
            printf("  Read module info...   "); fflush(stdout);
        }
        /* считываем информацию о модуле, которая хранится в его flash-памяти вместе с прошивкой */
        err = LTRAVR_ReadProgrammMemory(&hnd, (WORD*)f_info, (mpar->info_size+1)/2, mpar->info_offs/2);
        if ((err == LTR_OK) && ((mpar->userdata_size > 0) && (mpar->userdata_size <= MAX_USERDATA_SIZE))) {
            err = LTRAVR_ReadProgrammMemory(&hnd, (WORD*)f_userdata, (mpar->userdata_size+1)/2, mpar->userdata_offset/2);
        }
        if (err == LTR_OK) {
            /* проверяем ее CRC */            
            WORD crc_ev;
            WORD crc = (WORD)(((WORD)f_info[mpar->info_size-1] << 8) | f_info[mpar->info_size-2]);
            if (mpar->flags & MODULE_FLAGS_CRC_FFFF) {
                crc_ev = 0xFFFF;
            } else {
                crc_ev = eval_crc16(0, f_info, mpar->info_size-2);
                if (mpar->flags & MODULE_FLAGS_CRC_BE) {
                    crc = ((crc >> 8) & 0xFF) | ((crc << 8) &0xFF00);
                }
            }

            module_info_valid = crc == crc_ev;
            if (verbose) {                
                printf("%s: ", module_info_valid ? "ok" : "err");
                if (module_info_valid) {
                    printf("serial = %s, ", &f_info[mpar->serial_offset]);
                    f_print_firm_ver(f_info, mpar);
                } else {
                    printf("invalid flash info crc (0x%x), calculated crc = 0x%x\n", crc, crc_ev);
                }
                printf("\n");
                fflush(stdout);
            }
        } else {
            if (verbose) {
                printf("err\n"); fflush(stdout);
            }
            fprintf(stderr, "Error %d: %s!\n", err, LTRAVR_GetErrorString(err));
        }
    } else {
        if (verbose) {
            printf("err\n"); fflush(stdout);
        }
        fprintf(stderr, "Error %d: %s!\n", err, LTRAVR_GetErrorString(err));
    }

    if (err == LTR_OK) {
        INT close_err = LTR_OK;
        DWORD buf_size;
        DWORD file_size;
        BYTE *buf=NULL, *rbuf=NULL;

        /* получаем размер прошивки */
        fseek(f, 0, SEEK_END);
        file_size = ftell(f);
        fseek(f, 0, SEEK_SET);
        if ((file_size < (mpar->info_offs + mpar->info_size)) ||
                (file_size > mpar->flash_words*2)) {
            err = ERR_INVALID_FILE_SIZE;
            fprintf(stderr, "Invalid firmware flash filesize: %d\n", file_size);
        }


        if (err == LTR_OK) {
            buf_size = (DWORD)file_size;
            /* если пользовательская область больше файла прошивки, то должны расширить
             * буфер, чтобы вычитать в него также и пользовательскую область, так
             * как стирается всегда весь чип целиком */
            if ((mpar->userdata_offset + mpar->userdata_size) > buf_size) {
                buf_size = mpar->userdata_offset + mpar->userdata_size;
            }

            /* Так как прошивки небольшие, то выделяем для простоты
             * память сразу под всю прошивку и считываем ее из файла за раз.
             * Второй буфер используется для проверки правильности записи,
             * путем считывания содержимого памяти AVR и сравнения с тем что было записано */
            buf = malloc(buf_size);
            rbuf = malloc(buf_size);
            if ((buf == NULL) || (rbuf == NULL)) {
                err = LTR_ERROR_MEMORY_ALLOC;
                fprintf(stderr, "Memory allocation error!\n");
            }
        }

        if (err == LTR_OK) {
            /* считываем прошивку из файла */
            DWORD rsize = (WORD)fread(buf, 1, file_size, f);
            if (rsize!=file_size) {
                fprintf(stderr, "Read file Error!\n");
                err = ERR_FILE_READ;
            }
        }

        if (err == LTR_OK) {
            WORD crc;
            if (module_info_valid) {
                /* Копируем серийный номер и калибровочные коэффициенты, чтобы они остались неизменными
                 * после перезаписи в случае, если они присутствовали в модуле */
                if (mpar->serial_size)
                    memcpy(&buf[mpar->info_offs + mpar->serial_offset], &f_info[mpar->serial_offset], mpar->serial_size);
                if (mpar->cbr_size)
                    memcpy(&buf[mpar->info_offs + mpar->cbr_offset], &f_info[mpar->cbr_offset], mpar->cbr_size);
            }

#ifdef ENABLE_SET_SERIAL
            /* устанавливаем требуемый серийный номер */
            if (set_serial!=NULL) {
                strncpy((char*)&buf[mpar->info_offs + mpar->serial_offset], set_serial, mpar->serial_size);
                buf[mpar->info_offs + mpar->serial_offset + mpar->serial_size-1] = 0;
            }
#endif

            if (mpar->flags & MODULE_FLAGS_CRC_FFFF) {
                crc = 0xFFFF;
            } else {
                /* рассчитываем CRC для полученной информации */
                crc = eval_crc16(0, &buf[mpar->info_offs], mpar->info_size-2);

                if (mpar->flags & MODULE_FLAGS_CRC_BE) {
                    crc = ((crc >> 8) & 0xFF) | ((crc << 8) &0xFF00);
                }
            }

            buf[mpar->info_offs + mpar->info_size - 2] = crc & 0xFF;
            buf[mpar->info_offs + mpar->info_size - 1] = (crc>>8) & 0xFF;
        }

        /* если есть область пользовательских данных, то подменяем значения на считаные
         * из flash-памяти контроллера */
        if ((err == LTR_OK) && (mpar->userdata_size > 0) && (mpar->userdata_size <= MAX_USERDATA_SIZE))  {
            memcpy(&buf[mpar->userdata_offset], f_userdata, mpar->userdata_size);
        }

        /* стараем содержимое всей flash-памяти модуля (так как по частям у AVR нельзя) */
        if (err == LTR_OK) {
            if (verbose) {
                printf("  Chip erase...         "); fflush(stdout);
            }
            err = LTRAVR_ChipErase(&hnd);
            if (err == LTR_OK) {
                if (verbose) {
                    printf("ok.\n"); fflush(stdout);
                }
            } else {
                if (verbose) {
                    printf("err\n"); fflush(stdout);
                }
                fprintf(stderr, "Error %d: %s!\n", err, LTRAVR_GetErrorString(err));
            }
        }

        if (err == LTR_OK) {
            if (verbose) {
                printf("  Write firmware...     "); fflush(stdout);
            }

            /* запись самой прошивки (мб разбить на части, чтобы видеть прогресс) */
            err = LTRAVR_WriteProgrammMemoryCb(&hnd, (const WORD*)buf, buf_size/2, 0,
                                               f_write_prog_vb, NULL);
            if (err == LTR_OK) {
                if (verbose) {
                    printf("ok.\n"); fflush(stdout);
                }
            } else {
                if (verbose) {
                    printf("err\n"); fflush(stdout);
                }
                fprintf(stderr, "Error %d: %s!\n", err, LTRAVR_GetErrorString(err));
            }
        }

        if (err == LTR_OK) {
            if (verbose) {
                printf("  Verify firmware...    "); fflush(stdout);
            }
            /* считываем всю память и проверяем совпадение с тем, что записали */
            err =  LTRAVR_ReadProgrammMemory(&hnd, (WORD*)rbuf, buf_size/2, 0);
            if (err == LTR_OK) {
                WORD i;
                for (i=0; (err == LTR_OK) && (i < buf_size); i++) {
                    if (rbuf[i]!=buf[i]) {
                        if (verbose) {
                            printf("err\n"); fflush(stdout);
                        }
                        fprintf(stderr, "Error! Invalid byte %d: write 0x%x, read 0x%x\n", i, buf[i], rbuf[i]);
                        err = ERR_FIRM_VERIFY;
                    }
                }

                if (!err && verbose) {
                    printf("ok.\n"); fflush(stdout);
                }
            } else {
                if (verbose) {
                    printf("err\n"); fflush(stdout);
                }
                fprintf(stderr, "Error %d: %s!\n", err, LTRAVR_GetErrorString(err));
            }
        }

        free(rbuf);
        free(buf);

        if (verbose) {
            printf("  Close module...       "); fflush(stdout);
        }
        close_err = LTRAVR_Close(&hnd);
        if (close_err == LTR_OK) {
            if (verbose) {
                printf("ok.\n"); fflush(stdout);
            }
        } else {
            if (verbose) {
                printf("err\n"); fflush(stdout);
            }
            fprintf(stderr, "Error %d: %s!\n", close_err, LTRAVR_GetErrorString(close_err));
        }

        if (!err)
            err = close_err;
    }
    return err;
}



int main(int argc, char** argv) { t_ltrcfg_state st;
    int out, err;
    t_module_param* mpar=NULL;
#ifdef _WIN32
    setlocale(LC_ALL, "");
#endif

    memset(&st, 0, sizeof(st));

    /* разбор опций */
    err = f_parse_options(&st, argc, argv, &out);

    if (!err && !out) {
        FILE* f = fopen(st.filename, "rb");
        if (f==NULL) {
            err = ERR_OPEN_FILE;
            fprintf(stderr, "Error! Cannot open firmware file %s.\n", st.filename);
        }

        /* по прошивке пытаемся определить для какого она модуля */
        if (!err)
            err = f_get_info_from_firm(f, &mpar, st.verbose);

        if (!err && !out) {
            /* проходим все крейты в поисках нужных модулей для обновления */
            INT crates_ok=0, crates_err=0, modules_ok=0, modules_err = 0;
            /* устанавливаем управляющее соединение с сервером для получения
             * списка крейтов */
            TLTR hltr;


            LTR_Init(&hltr);
            err = LTR_OpenSvcControl(&hltr, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
            if (err) {
                fprintf(stderr, "Cannot open connection with ltrd. Error %d: %s!",
                        err, LTR_GetErrorString(err));
            } else {
                /* получаем список крейтов */
                char crates[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
                err = LTR_GetCrates(&hltr, (BYTE*)crates);
                if (err) {
                    fprintf(stderr, "Cannot get crate list from ltrd. Error %d: %s!",
                            err, LTR_GetErrorString(err));
                }

                if (!err) {
                    WORD i;
                    /* проходимся по всем крейтам, но если не было параметра --all,
                     * то завершаем после первого найденного модуля */
                    for (i=0; (i < LTR_CRATES_MAX) && (st.all || ((modules_err+modules_ok)==0)); i++) {
                        if ((crates[i][0]!=0) &&
                                ((st.csn[0]==0) || !strcmp(crates[i], st.csn))) {
                            /* для каждого действительного серийного номера
                               (если явно задан серийный - то только для него)
                               устанавливаем соединение, чтобы получить список модулей */
                            TLTR hcrate;
                            INT cr_err = LTR_OK;

                            LTR_Init(&hcrate);
                            cr_err = LTR_OpenCrate(&hcrate, hltr.saddr, hltr.sport,
                                                   LTR_CRATE_IFACE_UNKNOWN, crates[i]);
                            if (cr_err) {
                                fprintf(stderr, "Cannot open crate with serial = %s. Error %d: %s!",
                                    crates[i], cr_err, LTR_GetErrorString(cr_err));
                            } else {
                                WORD mids[LTR_MODULES_PER_CRATE_MAX];
                                cr_err = LTR_GetCrateModules(&hcrate, mids);
                                if (cr_err) {
                                    fprintf(stderr, "Cannot get modules list for crate %s. Error %d: %s!",
                                        hcrate.csn, cr_err, LTR_GetErrorString(cr_err));
                                } else {
                                    if (st.slot==0) {
                                        /* ищем модули, для которых подходит прошивка */
                                        INT m;
                                        for (m=0; m < LTR_MODULES_PER_CRATE_MAX; m++) {
                                            if (mids[m]==mpar->mid) {
                                                INT merr = f_write_firm(hcrate.csn, m+LTR_CC_CHNUM_MODULE1, f, mpar,
#ifdef ENABLE_SET_SERIAL
                                                                        st.set_serial,
#endif
                                                                        st.verbose);
                                                if (merr) {
                                                    modules_err++;
                                                } else {
                                                    modules_ok++;
                                                }

                                                if (!st.all)
                                                    break;
                                            }
                                        }
                                    } else {
                                        /* сверяем mid модуля в указанном слоте с mid, полученным
                                         * при анализе прошивки */
                                        INT merr;
                                        INT slot_i = st.slot-LTR_CC_CHNUM_MODULE1;
                                        if (mids[slot_i] == LTR_MID_EMPTY) {
                                            fprintf(stderr, "Error! Specified slot is empty!\n");
                                            merr = ERR_EMPTY_SLOT;
                                        } else if (mids[slot_i]==LTR_MID_IDENTIFYING) {
                                            fprintf(stderr, "Error! Module in specified slot is not identified!\n");
                                            merr = ERR_IDENTIFYING_SLOT;
                                        } else if (mids[slot_i]!=mpar->mid) {
                                            fprintf(stderr, "Error! Invalid module in slot. Module mid=0x%x, firmware for module with mid = 0x%x!\n",
                                                    mids[slot_i], mpar->mid);
                                            merr = ERR_INVALID_MID_IN_SLOT;
                                        } else {
                                            merr = f_write_firm(st.csn, st.slot, f, mpar,
#ifdef ENABLE_SET_SERIAL
                                                                st.set_serial,
#endif
                                                                st.verbose);
                                        }

                                        if (merr) {
                                            modules_err++;
                                        } else {
                                            modules_ok++;
                                        }
                                    }
                                }
                                LTR_Close(&hcrate);
                            }

                            if (cr_err) {
                                crates_err++;
                            } else {
                                crates_ok++;
                            }
                        }
                    }

                    if ((modules_ok + modules_err)==0) {
                        printf("Cannot find specified LTR-modules!\n");
                    } else {
                        printf("\nWork done!\n");
                        printf("   Total crates  processed: %d, successful: %d, with error: %d\n", crates_ok+crates_err, crates_ok, crates_err);
                        printf("   Total modules processed: %d, successful: %d, with error: %d\n", modules_ok+modules_err, modules_ok, modules_err);
                    }
                }
                LTR_Close(&hltr);
            }
        }

        if (f!=NULL)
            fclose(f);
    }
    return err;
}
